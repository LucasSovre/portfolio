const tagElem = document.getElementById("tag");
const bodyElem = document.body
const mainElem = document.getElementById("main"); 

function getColorFromPercent(percent) {

    if(percent >= 70){
        return "black";
    }

    var grayValue = Math.round(255 * (1 - percent / 100));
    var color = "rgb(" + grayValue + "," + grayValue + "," + grayValue + ")";
    return color;
}

function refresh() {
    if(document.documentElement.scrollWidth > 1200 ){
        var percentage = Math.abs(tagElem.getBoundingClientRect().top) * 100 / document.documentElement.scrollHeight;
        bodyElem.style.backgroundColor = getColorFromPercent(percentage);
        mainElem.style.width = 100-percentage.toFixed(1) +"%"
        mainElem.style.height = 100-(percentage.toFixed(1)/4)+"%"
        mainElem.style.marginTop = ((percentage.toFixed(1)/4)/4)+"%"
        if(percentage> 10){
            mainElem.style.borderRadius = "15px";
        }
        if(mainElem.offsetWidth < 1000){
            mainElem.classList.add('ipad')
        }else{mainElem.classList.remove('ipad')}
        if(percentage > 70){
            mainElem.classList.add('iphone')
            mainElem.style.marginRight = "50vw"
            document.getElementById('info').style.transform = "translateX(20vw)"
        }else{mainElem.classList.remove('iphone');mainElem.style.marginRight = "0";document.getElementById('info').style.transform = "translateX(100vw)"}
    }
}

setInterval(refresh, 100);

document.addEventListener("scroll", function() {
    refresh();
  });